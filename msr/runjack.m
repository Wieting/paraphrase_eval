addpath('classifyParaphrases/code');
inputFile = 'classifyParaphrases/parsed.txt';
%load('../../paraphrase_RNN/params/skipwiki25_phrases_100k_single_0.001_0.0001_filtered_100k.params3.mat');
load('../../paraphrase_RNN/bigrams/data/skipwiki25.mat');
load('../../paraphrase_RNN_words/params/skipwiki25_words_1e-05_500_words-xl.params3.mat');
convertStanfordParserTrees

test.allSNum = allSNum;
test.allSStr = allSStr;
test.allSTree = allSTree;
test.allSKids = allSKids;
%save('../test.mat','test');
%jack change
save('test.mat','test');

clear;

inputFile = 'classifyParaphrases/parsedtrain.txt';
%load('../../paraphrase_RNN/params/skipwiki25_phrases_100k_single_0.001_0.0001_filtered_100k.params3.mat');
load('../../paraphrase_RNN/bigrams/data/skipwiki25.mat');
load('../../paraphrase_RNN_words/params/skipwiki25_words_1e-05_500_words-xl.params3.mat');
theta = rand(25*25*2+25,1);
convertStanfordParserTrees

train.allSNum = allSNum;
train.allSStr = allSStr;
train.allSTree = allSTree;
train.allSKids = allSKids;
%save('../test.mat','test');
%jack change
save('train.mat','train');
simMat