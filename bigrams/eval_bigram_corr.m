addpath('../../paraphrase_RNN/single_W/code/core/');
load('../../paraphrase_RNN/single_W/code/test/data/skipwiki25.mat')

eval_bigram_corr2(25,words,'log_bigram_vn_test_skipwiki25.txt', 'data/test_verb-noun.txt','verb-noun');
eval_bigram_corr2(25,words,'log_bigram_vn_dev_skipwiki25.txt', 'data/dev_verb-noun.txt','verb-noun');

eval_bigram_corr2(25,words,'log_bigram_nn_test_skipwiki25.txt', 'data/test_noun-noun.txt','noun-noun');
eval_bigram_corr2(25,words,'log_bigram_nn_dev_skipwiki25.txt', 'data/dev_noun-noun.txt','noun-noun');

eval_bigram_corr2(25,words,'log_bigram_an_test_skipwiki25.txt', 'data/test_adj-noun.txt','adj-noun');
eval_bigram_corr2(25,words,'log_bigram_an_dev_skipwiki25.txt', 'data/dev_adj-noun.txt','adj-noun');