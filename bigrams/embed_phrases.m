function E=embed_phrases(m, v, w, include_concat, include_sum)
% m should be a map mapping words to their indices in the vocabulary.
% v should be the embedding vectors for *words*, ordered according to the vocabulary. Each word is a row vector.
% w should be the cell array of phrases to find embedding vectors for.
% if include_concat = 1, will include the concatenation of unigram embedding vectors to create phrase embedding 
% if include_sum = 1, will include sum of unigram embedding vectors in phrase embedding
% E is the matrix of phrase embedding vectors for all words in w, with one row vector per phrase in w.

if include_concat == 0 && include_sum == 0,
	fprintf('Warning: you specified no concat and no sum, so theres nothing left! Including concat.\n');
	include_concat = 1;
end
 
% Tokenize the phrases.
ss = repmat({'%s'},size(w, 1),1);
toks = cellfun(@strread, w, ss, 'UniformOutput', false);

ms = repmat({m},size(w, 1),1);
print_backoff_flags = repmat({0}, size(w,1),1);
F = cellfun(@lookup_softmatch, ms, toks, print_backoff_flags, 'UniformOutput', false);

nn = size(F,1);
num_words = size(F{1},2);
% If there's only one word, then we force this embedding to be concat only.
if num_words == 1,
	include_concat = 1;
	include_sum = 0;
end

dim = size(v,2);
wordcat_embed_length = 0;
if include_concat == 1,
	wordcat_embed_length = wordcat_embed_length + (dim*num_words);
end

phrase_embed_length = wordcat_embed_length;
if include_sum == 1,
	phrase_embed_length = phrase_embed_length + dim;
	%phrase_embed_length = phrase_embed_length + dim;
end
E = zeros(nn, phrase_embed_length);
for i=1:nn,
	if include_concat == 1,
		E(i,1:wordcat_embed_length) = reshape(v(F{i}',:)',1,[]);
	end
	if include_sum == 1,
		% Sum over the rows, producing a row vector where each entry is the sum over all rows of the values in that column.
		E(i,(wordcat_embed_length + 1):phrase_embed_length) = sum(v(F{i}',:),1);
		%E(i,(wordcat_embed_length + 1):(wordcat_embed_length + dim)) = sum(v(F{i}',:));
		%E(i,(wordcat_embed_length + dim + 1):phrase_embed_length) = prod(v(F{i}',:));
	end
	%reshape(v(F{i}',:)',1,[]);
	

	%for j=1:num_words,
	%	start_index = ((j - 1) * dim) + 1;
	%	end_index = (start_index + dim) - 1;
	%	E(i,start_index:end_index) = v(F{i}(j),:);
	%end
end


