function [] = eval_bigram_corr2(hiddenSize,words,outputf,dataf,type)

fid = fopen(outputf,'w');
count = 0;
files=dir('../../paraphrase_RNN/params/');
for k=1:length(files)
    fname = files(k).name
    
    if(length(fname) <= 2)
        continue;
    end
    
    if(isempty(strfind(fname,type)))
        continue
    end
    
    load(strcat('../../paraphrase_RNN/params/',fname));
    acc = bigrams(theta,We_orig,hiddenSize,words,dataf);
    fprintf(fid,'%s....%d\n',fname,acc);
    count = count + 1;
    if(count > 20)
        %break;
    end
end

end