
def writeFile(handle,lines):
    for i in lines:
        handle.write(i+"\n")


def getLine(l,type):
    l=l.split()
    s = l[3]+' '+l[4]+'\t'+l[5]+' '+l[6]+'\t'+l[7]
    if(type == 'test'):
        if 'verbobjects' in l:
            test.append(s)
            vtest.append(s)
        if('compoundnouns' in l):
            test.append(s)
            ntest.append(s)
        if('adjectivenouns' in l):
            test.append(s)
            atest.append(s)
    if(type == 'dev'):
        if 'verbobjects' in l:
            dev.append(s)
            vdev.append(s)
        if('compoundnouns' in l):
            dev.append(s)
            ndev.append(s)
        if('adjectivenouns' in l):
            dev.append(s)
            adev.append(s)

f = open('raw_data.txt','r')
lines = f.readlines()

test= []
dev = []

vtest = []
vdev = []

atest = []
adev = []

ntest = []
ndev = []

count =0
for i in lines:
    if count==0:
        count += 1
        test.append(i.strip())
        dev.append(i.strip())
        ntest.append(i.strip())
        atest.append(i.strip())
        vtest.append(i.strip())
        ndev.append(i.strip())
        adev.append(i.strip())
        vdev.append(i.strip())
        continue
    i=i.strip()
    num = i.split()[0].replace('participant','')
    num = int(num)
    if(num <= 108):
        getLine(i,'test')
    else:
        getLine(i,'dev')
    count += 1

#print len(ndev)
#print len(vdev)
#print len(adev)

#print len(ntest)
#print len(vtest)
#print len(atest)

fout = open('test_bigram.txt','w')
writeFile(fout,test)

fout = open('dev_bigram.txt','w')
writeFile(fout,dev)

fout = open('test_adj-noun.txt','w')
writeFile(fout,atest)

fout = open('test_noun-noun.txt','w')
writeFile(fout,ntest)

fout = open('test_verb-noun.txt','w')
writeFile(fout,vtest)

fout = open('dev_adj-noun.txt','w')
writeFile(fout,adev)

fout = open('dev_noun-noun.txt','w')
writeFile(fout,ndev)

fout = open('dev_verb-noun.txt','w')
writeFile(fout,vdev)

