import operator
import sys

f = open(sys.argv[1],'r')
lines = f.readlines()

d={}
newlines = []
for l in lines:
    i = l.split('....')
    n1 = float(i[1])
    l = i[0] + " "+str(n1)
    newlines.append(l)
    d[l] = n1

sorted_d = sorted(d.iteritems(), key=operator.itemgetter(1), reverse=True)
fout = open(sys.argv[1]+'.analyzed.txt','w')
for i in sorted_d:
    fout.write(i[0]+'\n')
