#alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab

cd bigrams
matlab -nodisplay -nodesktop -nojvm -nosplash -r "eval_bigram_corr;quit" &
#cd ../semeval
#matlab -nodisplay -nodesktop -nojvm -nosplash -r "eval_semeval_corr;quit" &
cd ../wordsim
matlab -nodisplay -nodesktop -nojvm -nosplash -r "eval_wordsim_corr;quit" &