addpath('../../paraphrase_RNN/single_W/code/core/');
%load('../../paraphrase_RNN/single_W/code/test/data/skipwiki25.mat')
load('../../paraphrase_RNN_words/params/skipwiki25_words_1e-05_500_words-xl.params3.mat')
c=semeval_baseline2([], We_orig, 25, words, '../../paraphrase_RNN/semeval/data/SICK_test_annotated.txt.mat');
c
%25.05 (no recursive average,25)
%26.98 ('' 50)
%21.33

%c=semeval_baseline_wordoverlap([], We_orig, 25, words, '../../paraphrase_RNN/semeval/data/SICK_test_annotated.txt.mat');
%c
%0.5493

%c=semeval_baseline_llm([], We_orig, 25, words, '../../paraphrase_RNN/semeval/data/SICK_test_annotated.txt.mat');
%c
%0.1972

%eval_semeval_corr2(25,words,'log_semeval_filtered_skipwiki25.txt', '../../paraphrase_RNN/semeval/data/SICK_test_annotated.txt.mat','_filtered');
%eval_semeval_corr2(25,words,'log_semeval_unfiltered_skipwiki25.txt', '../../paraphrase_RNN/semeval/data/SICK_test_annotated.txt.mat','_unfiltered');