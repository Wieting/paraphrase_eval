function [c] = semeval_baseline_wordoverlap(theta, We_orig, hiddenSize, words, dfile)

theta = rand(2*hiddenSize*hiddenSize+hiddenSize,1);
load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

for i=1:1:length(train_data)
    n1 = train_data{i}{1}.nums;
    n2 = train_data{i}{2}.nums;
    den = length(n1);
    if(length(n1) > length(n2))
        den = length(n2);
    end
    
    num = length(intersect(n1,n2));
    d = num / den;
    
    
    
    x = [x d];
    y = [y train_data{i}{3}];
end

[c,~] = corr(x',y','Type','Spearman');

end