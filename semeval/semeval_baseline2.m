function [c] = semeval_baseline2(theta, We_orig, hiddenSize, words, dfile)

theta = rand(2*hiddenSize*hiddenSize+hiddenSize,1);
load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

%change line in forwardpassWordDer
[train_data] = feedForwardTreesAdd(train_data, theta, hiddenSize, We_orig);

for i=1:1:length(train_data)
    g1 = getAvgVector(train_data{i}{1});
    g2 = getAvgVector(train_data{i}{2});
    %g1 = train_data{i}{1}.nodeFeaturesforward(:,end);
    %g2 = train_data{i}{2}.nodeFeaturesforward(:,end);
    d = sum(g1 .* g2);
    x = [x d];
    y = [y train_data{i}{3}];
end

[c,~] = corr(x',y','Type','Spearman');

end

function [v] = getAvgVector(t)

s = length(t.nums);
m= t.nodeFeaturesforward(:,1:s);
v = sum(m,2)/s;

end