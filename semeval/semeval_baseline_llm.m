function [c] = semeval_baseline_llm(theta, We_orig, hiddenSize, words, dfile)

theta = rand(2*hiddenSize*hiddenSize+hiddenSize,1);
load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

for i=1:1:length(train_data)
    n1 = train_data{i}{1}.nums;
    n2 = train_data{i}{2}.nums;
    d=0;
    if(length(n1) > length(n2))
        d = llm(train_data{i}{2}.nodeFeaturesforward(:,1:length(n2)), train_data{i}{1}.nodeFeaturesforward(:,1:length(n1)));
    else
        d = llm(train_data{i}{1}.nodeFeaturesforward(:,1:length(n1)), train_data{i}{2}.nodeFeaturesforward(:,1:length(n2)));
    end
    
    x = [x d];
    y = [y train_data{i}{3}];
end

[c,~] = corr(x',y','Type','Spearman');

end

function [score] = llm(small, large)
[m n] = size(small);
[~,n2] = size(large);

total = 0;

ct =0;
for i=1:1:n
    max = -1;
    v1 = small(:,i);
    for j=1:1:n2
        v2 = large(:,j);
        dd = sum(v1.*v2)/(norm(v1)*norm(v2));
        if(dd > max)
            max = dd;
        end
    end
    
    total = total + max;
    ct = ct + 1;
end

score = max / ct;
end