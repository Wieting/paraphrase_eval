function [c] = semeval(theta, We_orig, hiddenSize, words, dfile)

load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

[train_data] = feedForwardTrees(train_data, theta, hiddenSize, We_orig);

for i=1:1:length(train_data)
    g1 = train_data{i}{1}.nodeFeaturesforward(:,end);
    g2 = train_data{i}{2}.nodeFeaturesforward(:,end);
    d = sum(g1 .* g2);
    x = [x d];
    y = [y train_data{i}{3}];
end

[c,~] = corr(x',y','Type','Spearman');

end